import requests
from bs4 import BeautifulSoup
import sendEmail

sender_email = "pythonprojects99@gmail.com"
receiver_email = "mart.seedre@gmail.com"

url = 'https://explorep2p.com/mintos-lender-ratings/'
headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) "
                         "Chrome/74.0.3729.169 Safari/537.36"}

page = requests.get(url, headers=headers)

soup = BeautifulSoup(page.content, 'html.parser')
results = soup.find_all(class_='elementor-heading-title elementor-size-default')

last_updated = results[0]

f = open("lastUpdated.txt","r+")

last_updated_from_file = f.read()

if last_updated.text == last_updated_from_file:
    print("no new updates")

else:
    print("there has been an update")
    # TODO: send email notification
    f.seek(0)
    f.write(last_updated.text)
    f.truncate()
    sendEmail.sendEmail(sender_email, receiver_email, 'Change in P2P rating webpage',
                        'Go check https://explorep2p.com/mintos-lender-ratings/')

f.close


#
#f.close

#print(last_updated.text)